FROM python:3.7
MAINTAINER essanpupil
ADD . /usr/src/app
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
RUN python manage.py collectstatic
EXPOSE 8000
ENV DJANGO_SETTINGS_MODULE='proman.settings_prod'
RUN python manage.py migrate
CMD exec gunicorn proman.wsgi:application --bind 0.0.0.0:8000 --workers 3