from proman.settings import *

ALLOWED_HOSTS = ['*']
DEBUG = False
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'promandb',
        'PASSWORD': 'adminpasswd',
        'PORT': '5432',
        'USER': 'tasker',
        'HOST': '192.168.33.10',
    }
}
